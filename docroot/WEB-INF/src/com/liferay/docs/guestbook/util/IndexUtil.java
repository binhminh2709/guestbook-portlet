package com.liferay.docs.guestbook.util;

import java.util.List;

import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.ParseException;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.TermQuery;
import com.liferay.portal.kernel.search.TermQueryFactoryUtil;
import com.liferay.portal.kernel.search.TermRangeQuery;
import com.liferay.portal.kernel.search.TermRangeQueryFactoryUtil;

public class IndexUtil {
	public static void main(String[] args) {
		
		List<Indexer> indexers = IndexerRegistryUtil.getIndexers();
		
		for (Indexer indexer : indexers) {
			System.out.println("portletId: " + indexer.getPortletId());
			
			String[] classnames = indexer.getClassNames();
			
			for (String classname : classnames) {
				System.out.println("classname: " + classname);
			}
			
			System.out.println();
		}
		
		// --------------TermQuery
		SearchContext searchContext = null;
		TermQuery termQuery = TermQueryFactoryUtil.create(searchContext, "title", "liferay");
		try {
			Hits hits = SearchEngineUtil.search(searchContext, termQuery);
		} catch (SearchException se) {
			// handle search exception
		}
		
		// ------------------TermRangeQuery
		TermRangeQuery termRangeQuery1 = TermRangeQueryFactoryUtil
				.create(searchContext, "modified", "201412040000000", "201412050000000", true, true);
		try {
			Hits hits = SearchEngineUtil.search(searchContext, termRangeQuery1);
			Document[] docs = hits.getDocs();
			for (Document doc : docs) {
			}
			
			List<Document> listdocs = hits.toList();
			
		} catch (SearchException se) {
			// handle search exception
		}
		
		try {
			// -------------------BooleanQuery
			TermQuery termQuery1 = TermQueryFactoryUtil.create(searchContext, "title", "liferay");
			TermQuery termQuery2 = TermQueryFactoryUtil.create(searchContext, "description", "lucene");
			TermRangeQuery termRangeQuery = TermRangeQueryFactoryUtil.create(searchContext, "modified", "201412040000000", "201412050000000", true,
					false);
			
			BooleanQuery booleanQuery = BooleanQueryFactoryUtil.create(searchContext);
			
			booleanQuery.add(termQuery1, BooleanClauseOccur.MUST);
			booleanQuery.add(termQuery2, BooleanClauseOccur.MUST_NOT);
			booleanQuery.add(termRangeQuery, BooleanClauseOccur.MUST);
			Hits hits = SearchEngineUtil.search(searchContext, booleanQuery);
		} catch (SearchException | ParseException se) {
			// handle search exception
		}
	}
}
